export interface Articulo {
    id: number;
    nombre: string;
    imagen: string;
    descripcion: string;
    activo: boolean;
    uuid: string;
    
}