import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ArticuloService } from './articulo.service';
import { HttpClientModule } from '@angular/common/http';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { CarouselComponent } from './carousel/carousel.component';
import { StickyButtonComponent } from './sticky-button/sticky-button.component';


@NgModule({
  declarations: [
    AppComponent,
    TopnavbarComponent,
    CarouselComponent,
    StickyButtonComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,MDBBootstrapModule.forRoot()
  ],
  providers: [ArticuloService],
  bootstrap: [AppComponent]
})
export class AppModule { }
