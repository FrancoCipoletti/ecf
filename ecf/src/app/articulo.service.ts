import { Injectable } from "@angular/core";
import { Observable} from "rxjs";
import { Articulo } from "./articulo";
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})

export class ArticuloService {

    private apiServerUrl=environment.apiBaseUrl;

    constructor(private http: HttpClient) { }

    public getArticulos(): Observable<Articulo[]> {
        return this.http.get<Articulo[]>(`${this.apiServerUrl}/articulo/all`);
    }   

    public addArticulo(articulo: Articulo): Observable<Articulo> {
        return this.http.post<Articulo>(`${this.apiServerUrl}/articulo/add`,articulo);
    }  

    public updateArticulo(articulo: Articulo): Observable<Articulo> {
        return this.http.put<Articulo>(`${this.apiServerUrl}/articulo/update`,articulo);
    }  

    public getArticulosActivos(): Observable<Articulo[]> {
        return this.http.get<Articulo[]>(`${this.apiServerUrl}/articulo/activos`);
    }  

    public getArticuloById(articuloId: number): Observable<Articulo> {
        return this.http.get<Articulo>(`${this.apiServerUrl}/articulo/${articuloId}`);
    }  
}