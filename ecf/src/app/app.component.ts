import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Articulo } from './articulo';
import { ArticuloService } from './articulo.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'ecf';
  public articulos : Articulo[];

  constructor (private articuloService: ArticuloService){ }

  ngOnInit(){
    this.getArticulos();
  }
  public getArticulos(): void {
    this.articuloService.getArticulos().subscribe (
      (response: Articulo[]) =>  { 
        this.articulos = response;
      },
   (error: HttpErrorResponse ) => {
     alert(error.message);
   }
  );
}
}